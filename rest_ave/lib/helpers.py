# -*- coding: utf-8 -*-
"""Template Helpers used in rest-ave."""
from cgi import FieldStorage
import random
import string

from tg import abort

import logging
from markupsafe import Markup
from datetime import datetime

from rest_ave import model

log = logging.getLogger(__name__)


def current_year():
    now = datetime.now()
    return now.strftime('%Y')


def represents_int(s):
    """Return if string represent an integer"""
    try:
        int(s)
        return True
    except ValueError:
        return False


def represents_bool(b):
    """Return if string represent a boolean"""
    return b in ('True', 'False')


def is_image(i):
    return True if isinstance(i, FieldStorage) else False


def valid_tags(tags):
    _valid_tags = []
    for t in tags:
        if t == '':
            continue
        tag = model.DBSession.query(model.Tag).filter(model.Tag.name == t).one_or_none()
        if not tag:
            abort(400, detail='User is not allowed to add tags', passthrough='json')
        _valid_tags.append(tag)
    return _valid_tags


def random_string(n):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(n))


def icon(icon_name):
    return Markup('<i class="glyphicon glyphicon-%s"></i>' % icon_name)


# Import commonly used helpers from WebHelpers2 and TG
from tg.util.html import script_json_encode

try:
    from webhelpers2 import date, html, number, misc, text
except SyntaxError:
    log.error("WebHelpers2 helpers not available with this Python Version")
