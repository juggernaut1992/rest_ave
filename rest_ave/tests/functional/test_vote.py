# -*- coding: utf-8 -*-
"""
Integration tests for Vote.

"""
import transaction
from tg import config

from rest_ave import model
from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header


class TestVote(TestController):
    """
    Testing Vote.
    """
    def setUp(self):
        super(TestVote, self).setUp()

        # Registering an account
        account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.account_post_resp = self.app.post_json('/users/register', params=account, headers=make_auth_header()).json
        valid_login = {
            'username': account['username'],
            'password': account['password']
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

        # Posting a question
        # Posting a valid question (tag is already added in the setup)
        valid_question = {
            'post_type': 'Question',
            'parent_id': None,
            'title': 'test',
            'description': 'testing',
            'tags': 'tag,',
        }
        self.question_post_resp = self.app.post_json('/posts/', params=valid_question, headers=self.headers).json

        # Registering another account
        account2 = {
            'username': 'test2',
            'password': 'test',
            'email_address': 'test2@test.com',
            'bio': 'tester'
        }
        self.account_post_resp2 = self.app.post_json('/users/register', params=account2, headers=make_auth_header()).json
        valid_login2 = {
            'username': account2['username'],
            'password': account2['password']
        }
        login_resp2 = self.app.post_json('/users/login', params=valid_login2, headers=make_auth_header()).json
        self.headers2 = make_auth_header(account_id=login_resp2['user']['id'], session_id=login_resp2['session'])
        model.DBSession.query(model.Account).filter(model.Account.id == self.headers2['account']).one().reputation = \
            config.get('min_rep_down')
        transaction.commit()
        # Registering another account
        account3 = {
            'username': 'test3',
            'password': 'test',
            'email_address': 'test3@test.com',
            'bio': 'tester'
        }
        self.account_post_resp3 = self.app.post_json('/users/register', params=account3, headers=make_auth_header()).json
        valid_login3 = {
            'username': account3['username'],
            'password': account3['password']
        }
        login_resp3 = self.app.post_json('/users/login', params=valid_login3, headers=make_auth_header()).json
        self.headers3 = make_auth_header(account_id=login_resp3['user']['id'], session_id=login_resp3['session'])

    def test_vote(self):
        """Testing Vote"""
        # upvote and check (as account2)
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Up'),
            headers=self.headers2
        )
        post = self.app.get('/posts/{}'.format(self.question_post_resp['id']), headers=self.headers2).json['post']
        assert len(post['votes']) == 1
        assert post['votes'][0].get('account_id') == self.account_post_resp2['id']
        assert post['votes'][0].get('post_id') == self.question_post_resp['id']

        # downvote with same user and check
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Down'),
            headers=self.headers2
        )
        post = self.app.get('/posts/{}'.format(self.question_post_resp['id']), headers=self.headers2).json['post']
        assert len(post['votes']) == 0

        # vote twice with same value
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Up'),
            headers=self.headers2
        )
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Up'),
            headers=self.headers2,
            status=400
        )
        # account3 upvotes
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Up'),
            headers=self.headers3
        )

        # Account2 voted in previous 10 line so we must have two votes for this question
        post = self.app.get('/posts/{}'.format(self.question_post_resp['id']), headers=self.headers2).json['post']
        assert len(post['votes']) == 2

        # vote with invalid value
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Invalid'),
            headers=self.headers2,
            status=400
        )
        # vote on non-existing post
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(1423423423, 'Up'),
            headers=self.headers2,
            status=404
        )
        # vote on your own post
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Up'),
            headers=self.headers,
            status=409
        )
