# -*- coding: utf-8 -*-
from tg import config
import redis
from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header


class TestNotification(TestController):
    """
    Test Notification.

    """
    # application_under_test = 'main'
    def setUp(self):
        super(TestNotification, self).setUp()
        self.r = redis.StrictRedis(db=config.get('redis_notif_db'))
        # Registering two accounts and a post
        self.actor = {
            'username': 'actor',
            'password': 'actor',
            'email_address': 'actor@test.com',
            'bio': 'actor'
        }
        self.actor_resp = self.app.post_json('/users/register', params=self.actor, headers=make_auth_header()).json
        self.actee = {
            'username': 'actee',
            'password': 'actee',
            'email_address': 'actee@test.com',
            'bio': 'actee'
        }
        self.actor_login = {
            'username': self.actor['username'],
            'password': self.actor['password']
        }
        self.actee_login = {
            'username': self.actee['username'],
            'password': self.actee['password']
        }
        self.actor_login_resp = self.app.post_json(
            '/users/login', params=self.actor_login, headers=make_auth_header()
        ).json
        self.actee_resp = self.app.post_json(
            '/users/register', params=self.actee, headers=make_auth_header()
        ).json
        self.actee_login_resp = self.app.post_json(
            '/users/login', params=self.actee_login, headers=make_auth_header()
        ).json
        self.valid_question = {
            'post_type': 'Question',
            'parent_id': None,
            'title': 'test',
            'description': 'testing',
            'tags': 'tag,',
        }
        self.actor_headers = make_auth_header(
            account_id=self.actor_login_resp['user']['id'],
            session_id=self.actor_login_resp['session']
        )
        self.actee_headers = make_auth_header(
            account_id=self.actee_login_resp['user']['id'],
            session_id=self.actee_login_resp['session']
        )
        # Actee posts a question
        self.question_resp = self.app.post_json('/posts', params=self.valid_question, headers=self.actee_headers).json

    def test_notification(self):
        """Testing Notification"""

        # Actor leaves an answer on actee's question
        valid_answer = {
            'post_type': 'Answer',
            'parent_id': self.question_resp['id'],
            'description': 'this is an answer',
        }
        self.answer_resp = self.app.post_json('/posts', params=valid_answer, headers=self.actor_headers).json

        # Check to see if actee has a notification
        resp = self.app.get('/users/notifications/{}'.format(self.actee_resp['id']), headers=self.actee_headers).json

        assert resp['notifications'][0]['text'] == '{} left an {} for you'.format(
            self.actor_login_resp['user']['username'],
            valid_answer['post_type'],
            self.valid_question['post_type']
        )
        assert self.r.get(self.actee_login_resp['user']['id']).decode("utf-8") == resp['notifications'][0]['text']

        # Actee behaves as an actor and leaves an answer on his own question, we don't expect notification
        valid_answer['description'] = 'this is a self-answer'  # Avoiding description uniqueness exception
        self.app.post_json('/posts', params=valid_answer, headers=self.actee_headers)

        # Check to see if actee has a notification
        resp = self.app.get('/users/notifications/{}'.format(self.actee_resp['id']), headers=self.actee_headers).json

        assert len(resp['notifications']) == 1  # Which is the previous one

    def tearDown(self):
        super(TestNotification, self).tearDown()
        self.r.delete(self.actee_login_resp['user']['id'])
