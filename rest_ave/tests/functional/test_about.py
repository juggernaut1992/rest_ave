# -*- coding: utf-8 -*-
"""
Integration tests for the AboutUs.

"""
from os import path

from tg import config

import rest_ave
from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header

PATH_TO_IMAGE = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'image.jpg'))


class TestAboutUs(TestController):
    def setUp(self):
        super(TestAboutUs, self).setUp()
        # Registering an account
        self.account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.account_post_resp = self.app.post_json('/users/register', params=self.account, headers=make_auth_header()).json

        valid_login = {
            'username': 'test',
            'password': 'test'
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

    def test_about_us(self):
        """Testing about us"""
        name = config.get('app.name')
        description = config.get('app.description')
        author = config.get('app.author')
        certificate = config.get('app.certificate')
        resp = self.app.get('/about_us', headers=self.headers).json
        assert name == resp['name']
        assert description == resp['description']
        assert author == resp['author']
        assert certificate == resp['certificate']
