# -*- coding: utf-8 -*-
from base64 import b64encode

from tg import config
from pyDes import triple_des

from rest_ave.tests import TestController


class TestAuthentication(TestController):
    # application_under_test = 'main'

    def setUp(self):
        super(TestAuthentication, self).setUp()

    def test_authentication(self):
        """Testing Authentication"""

        #
        secret_key = config.get('auth_secret_key')
        auth_message = config.get('auth_message')
        auth = {
            'token': b64encode(bytes(triple_des(secret_key).encrypt(auth_message, padmode=2)))
        }
        # Testing the pyDes functionality only
        self.app.get('/test_auth', headers=auth)

        # No token
        self.app.get('/test_auth', status=401)

        # Invalid token length
        auth = {
            'token': b'aKY2oNeA7RoGuTDNkhp5G9w=='
        }
        self.app.get('/test_auth', headers=auth, status=401)

        # Invalid token
        auth = {
            'token': b'aKY2oNeA7RoGuTDNkhp5G9x=='
        }
        self.app.get('/test_auth', headers=auth, status=401)

        # Testing session
        # Registering an account
        auth = {
            'token': b64encode(bytes(triple_des(secret_key).encrypt(auth_message, padmode=2)))
        }
        account = {
            'username': 'test2',
            'password': 'test2',
            'email_address': 'test@test.com2',
            'bio': 'tester2'
        }
        acc_id = self.app.post_json('/users/register', params=account, headers=auth).json['id']
        # Logging in
        params = {
            'username': account.get('username'),
            'password': account.get('password')
        }
        session = self.app.post_json('/users/login', params=params, headers=auth).json['session']
        # Get account with valid session and account_id in headers
        auth['session'] = session
        auth['account'] = str(acc_id)

        self.app.get('/accounts/{}'.format(acc_id), headers=auth)

        # Invalid session
        auth['session'] = 'invalid'
        self.app.get('/accounts/{}'.format(acc_id), headers=auth, status=403)

        # Invalid account
        auth['session'] = session
        auth['account'] = '34242343'
        self.app.get('/accounts/{}'.format(acc_id), headers=auth, status=403)

        # No session at all
        auth['account'] = str(acc_id)
        del auth['session']
        self.app.get('/accounts/{}'.format(acc_id), headers=auth, status=403)

        # TODO: No account at all
