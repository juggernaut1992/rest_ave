# -*- coding: utf-8 -*-
from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header


class TestSearch(TestController):

    def setUp(self):
        super(TestSearch, self).setUp()
        self.account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.app.post_json('/users/register', params=self.account, headers=make_auth_header())
        valid_login = {
            'username': self.account['username'],
            'password': self.account['password']
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

        self.valid_question = {
            'post_type': 'Question',
            'parent_id': None,
            'title': 'what is the meaning of cross-connect',
            'description': 'this is the description of the valid',
            'tags': 'tag,',
        }
        self.app.post_json('/posts/', params=self.valid_question, headers=self.headers)

    # application_under_test = 'main'

    def test_search(self):
        """Testing Search"""
        # Case where searching for post when query is in title
        search_input = {
            'query': 'cross'
        }
        search_resp = self.app.get(
            '/search?query={}&_for={}'.format(search_input['query'], 'post'),
            headers=self.headers
        ).json['result']
        assert len(search_resp) == 1
        assert search_resp[0]['title'] == self.valid_question['title']

        # Case where searching for post when query is in description
        search_input = {
            'query': 'valid'
        }
        search_resp = self.app.get(
            '/search?query={}&_for={}'.format(search_input['query'], 'post'),
            headers=self.headers
        ).json['result']
        assert len(search_resp) == 1
        assert search_resp[0]['title'] == self.valid_question['title']

        # searching for account with exact username
        search_input = {
            'query': self.account['username']
        }
        search_resp = self.app.get(
            '/search?query={}&_for={}'.format(search_input['query'], 'account'),
            headers=self.headers
        ).json['result']
        assert len(search_resp) == 1
        assert search_resp[0]['username'] == self.account['username']

        # searching for account with partial username
        search_input = {
            'query': self.account['username'][:2]
        }
        search_resp = self.app.get(
            '/search?query={}&_for={}'.format(search_input['query'], 'account'),
            headers=self.headers
        ).json['result']
        assert len(search_resp) == 1
        assert search_resp[0]['username'] == self.account['username']

        self.app.get(
            '/search?query={}&_for={}'.format(search_input['query'], 'invalid'),
            headers=self.headers,
            status=400
        )
