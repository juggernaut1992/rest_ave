# -*- coding: utf-8 -*-
import transaction
from rest_ave.tests import TestController
from rest_ave import model
from rest_ave.tests.helpers import make_auth_header


class TestTag(TestController):
    """
    Test Tags Existence.

    """
    # application_under_test = 'main'

    def setUp(self):
        super(TestTag, self).setUp()
        # Registering an account
        account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.account_post_resp = self.app.post_json('/users/register', params=account, headers=make_auth_header()).json
        valid_login = {
            'username': 'test',
            'password': 'test'
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

    def test_tag(self):
        """Testing Tags"""
        # Adding additional tag than the tag that is added in test setup
        model.DBSession.add(model.Tag(name='another_tag'))
        transaction.commit()
        resp = self.app.get('/tags', headers=self.headers).json
        assert len(resp['tags']) == 2
