# -*- coding: utf-8 -*-
"""
Integration tests for the User Interaction. e.g Logging in.

"""
from nose.tools import eq_
from tg import config

from rest_ave.tests import TestController
from rest_ave.tests.helpers import keep_keys, make_auth_header
from redis import StrictRedis


class TestUser(TestController):
    """
    Tests for the User Controller.

    """
    def setUp(self):
        super(TestUser, self).setUp()
        self.r = StrictRedis(db=config.get('redis_session_db'))
        # Posting an account for login purposes
        self.valid_account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.app.post_json('/users/register', params=self.valid_account, headers=make_auth_header())

    application_under_test = 'main'

    def test_user(self):
        """Testing Registration and Logging in and Logging out"""
        # login to the account posted
        valid_login = {
            'username': self.valid_account['username'],
            'password': self.valid_account['password']
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        eq_(
            keep_keys(['username', 'bio'], self.valid_account),
            keep_keys(['username', 'bio'], login_resp['user'])
        )
        # assert there is a session_token
        assert login_resp['session'] is not None

        headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

        # Logging out
        self.app.get('/users/logout', headers=headers)

        # Get yourself after logging out
        self.app.get('/accounts/{}'.format(login_resp['user']['id']), headers=headers, status=403)

        # Also make sure session is wiped out
        assert self.r.get(login_resp['user']['id']) is None


        """Going BlackBox"""
        # Invalid username
        invalid_username = {
            'username': 'invalid',
            'password': 'test'
        }
        self.app.post_json('/users/login', params=invalid_username, headers=make_auth_header(),  status=404)

        # Invalid password
        invalid_password = {
            'username': 'test',
            'password': 'invalid'
        }
        self.app.post_json('/users/login', params=invalid_password, headers=make_auth_header(), status=401)

        # Invalid request params
        invalid_login = {
            'username': 'test'
        }
        self.app.post_json('/users/login', params=invalid_login, headers=make_auth_header(), status=400)

        invalid_login2 = {
            'userrname': 'test',
            'password': 'test'
        }
        self.app.post_json('/users/login', params=invalid_login2, headers=make_auth_header(), status=400)

        # Invalid Json format
        self.app.post_json('/users/login', params=[], headers=make_auth_header(), status=400)

