from os import path
from datetime import datetime

import transaction
from tg import config

import rest_ave
from rest_ave.tests import TestController
from rest_ave.model.account import Account
from rest_ave.tests.helpers import make_auth_header
from rest_ave import model

PATH_TO_IMAGE = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'image.jpg'))


"""
Restrictions to be imposed:
    1. User must have at least ${config.get('min_down_rep')} reputation to be able to down-vote
    2. User must have at least ${config.get('min_com_rep')} reputation to be able to comment unless on his own post
    3. User must be official to be able to post feed
    4. User must be official to be able to post official document
    5. Official accounts are allowed to post free ads
"""


class TestRestrictions(TestController):
    """
    Testing Restrictions.
    """
    def setUp(self):
        super(TestRestrictions, self).setUp()

        # Registering an account
        account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.account_post_resp = self.app.post_json('/users/register', params=account, headers=make_auth_header()).json
        valid_login = {
            'username': account['username'],
            'password': account['password']
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

        # Posting a question
        # Posting a valid question (tag is already added in the setup)
        valid_question = {
            'post_type': 'Question',
            'parent_id': None,
            'title': 'test',
            'description': 'testing',
            'tags': 'tag,',
        }
        self.question_post_resp = self.app.post_json('/posts/', params=valid_question, headers=self.headers).json

        # Registering another account
        account2 = {
            'username': 'test2',
            'password': 'test',
            'email_address': 'test2@test.com',
            'bio': 'tester'
        }
        self.account_post_resp2 = self.app.post_json(
            '/users/register',
            params=account2,
            headers=make_auth_header()
        ).json
        valid_login2 = {
            'username': account2['username'],
            'password': account2['password']
        }
        login_resp2 = self.app.post_json('/users/login', params=valid_login2, headers=make_auth_header()).json
        self.headers2 = make_auth_header(account_id=login_resp2['user']['id'], session_id=login_resp2['session'])

    def test_restrictions(self):
        """Testing restrictions"""
        # User must have at least ${config.get('min_down_rep')} reputation to be able to down-vote

        # account2 tries to down-vote question posted by account1 (kheily saadeii!)
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Down'),
            headers=self.headers2,
            status=403
        )
        # Now give him some rep and try to succeed
        model.DBSession.query(Account).filter(Account.id == self.headers2['account']).one().reputation = \
            config.get('min_rep_down')
        transaction.commit()
        # Try again
        self.app.get(
            '/posts/vote?post_id={}&value={}'.format(self.question_post_resp['id'], 'Down'),
            headers=self.headers2,
        )
        # Get the post to see if it has a down-vote
        resp = self.app.get('/posts/{}'.format(self.question_post_resp['id']), headers=self.headers).json['post']
        assert len(resp['votes']) == 1
        assert resp['votes'][0]['value'] == 'Down'

        # User must have at least 15 reputation to be able to commit unless on his own post!
        # Roll back
        model.DBSession.query(Account).filter(Account.id == self.headers2['account']).one().reputation = 0
        transaction.commit()

        # Account2 tries to post comment on account1's question
        valid_comment = {
            'post_type': 'Comment',
            'parent_id': self.question_post_resp['id'],
            'description': 'this is a comment',
        }
        self.app.post_json('/posts/', params=valid_comment, headers=self.headers2, status=403)

        model.DBSession.query(Account).filter(Account.id == self.headers2['account']).one().reputation = \
            config.get('min_com_rep')
        transaction.commit()

        # Try again with requirement satisfied
        self.app.post_json('/posts/', params=valid_comment, headers=self.headers2)

        # Check to see if the question posted by account1 has a comment
        resp = self.app.get('/posts/{}'.format(self.question_post_resp['id']), headers=self.headers).json['comments']
        assert len(resp) == 1

        # Account1 as a non-official account tries to post a feed
        valid_feed = {
            'title': 'feed',
            'description': 'description feed',
            'image': None
        }
        self.app.post('/feeds/', params=valid_feed, headers=self.headers, status=403)

        model.DBSession.query(Account).filter(Account.id == self.headers['account']).one().is_official = True
        model.DBSession.query(Account).filter(Account.id == self.headers['account']).one().official_start = \
            datetime.now()
        transaction.commit()

        # Try again with requirement satisfied
        self.app.post('/feeds/', params=valid_feed, headers=self.headers)

        # Get all feeds
        assert len(self.app.get('/feeds/get_feeds', headers=self.headers).json['feeds']) == 1

        # TODO: number 4
        # assert False is True
