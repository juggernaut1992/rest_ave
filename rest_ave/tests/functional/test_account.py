# -*- coding: utf-8 -*-
"""
Integration tests for the Account.

"""
from os import path

from nose.tools import assert_equal

import rest_ave
from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header


PATH_TO_IMAGE = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'image.jpg'))
PATH_TO_DOCUMENT = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'document.pdf'))
PATH_TO_INVALID_IMAGE = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'too_large.jpg'))
PATH_TO_LARGE_DOCUMENT = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'too_large.pdf'))
PATH_TO_INVALID_DOCUMENT = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'mime.invalid'))
TEST_STORAGE = path.abspath(path.join(path.dirname(rest_ave.__file__), 'public', 'test_storage'))


class TestAccount(TestController):
    """
    Tests for the Account Controller.

    """
    application_under_test = 'main'

    def test_account(self):
        """Testing Getting Account, Uploading Image, Document"""

        """WhiteBox Testing"""

        # Posting a valid account
        viewer_account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        viewee_account = {
            'username': 'test2',
            'password': 'test2',
            'email_address': 'test@test.com2',
            'bio': 'tester2'
        }
        self.app.post_json('/users/register', params=viewer_account, headers=make_auth_header())
        viewee_resp = self.app.post_json('/users/register', params=viewee_account, headers=make_auth_header()).json
        valid_login = {
            'username': viewer_account['username'],
            'password': viewer_account['password']
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

        # Get viewee account to check if its view increases
        get_resp = self.app.get('/accounts/%s' % viewee_resp['id'], headers=self.headers).json

        assert_equal(viewee_account['username'], get_resp['account']['username']),
        assert_equal(viewee_account['bio'], get_resp['account']['bio'])
        assert_equal(get_resp['account']['reputation'], 0)
        assert_equal(len(get_resp['account']['viewers']), 1)

        # Get it again to make sure the view doesn't increase
        get_resp = self.app.get('/accounts/%s' % viewee_resp['id'], headers=self.headers).json['account']
        assert_equal(len(get_resp['viewers']), 1)

        # Edit account bio
        valid_put = {
            'id': login_resp['user']['id'],
            'bio': 'new bio'
        }
        self.app.put_json('/accounts/', params=valid_put, headers=self.headers)

        # Get to make sure it is edited
        get_resp_for_put = self.app.get('/accounts/%s' % login_resp['user']['id'], headers=self.headers).json['account']
        assert_equal(get_resp_for_put['bio'], valid_put['bio'])

        # Non-existing id
        self.app.get('/accounts/234234234', headers=self.headers, status=404)

        # Invalid id
        self.app.get('/accounts/invalid', headers=self.headers, status=400)

        # Put with very large bio_length (more than 100)
        invalid_put = {
            'id': login_resp['user']['id'],
            'bio': '''Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. '
                   'Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus '
                   'mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa '
                   'quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, '
                   'rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. '
                   'Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. '
                   'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus '
                   'in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut '''
        }
        self.app.put_json('/accounts/', params=invalid_put, headers=self.headers, status=400)

        # Put with invalid params
        invalid_put = {
            'id': login_resp['user']['id'],
            'bisdo': 'new bio'
        }
        self.app.put_json('/accounts/', params=invalid_put, headers=self.headers, status=400)

        # Put with no params
        invalid_put = {}
        self.app.put_json('/accounts/', params=invalid_put, headers=self.headers, status=400)

        # Put with extra params
        invalid_put = {
            'id': login_resp['user']['id'],
            'bio': 'new bio',
            'extra': 'I am extra'
        }
        self.app.put_json('/accounts/', params=invalid_put, headers=self.headers, status=400)

        # Post an account_image for the account just posted
        with open(PATH_TO_IMAGE, 'rb') as image:
            files = [('image', 'username', image.read())]
            self.app.post(
                '/accounts/upload_image/',
                upload_files=files,
                headers=self.headers
            )

        """ Upload another picture (First get the profile image, then change it, then check to see if the previous
        file has been removed (No reason to keep it)
        """
        acc = self.app.get('/accounts/{}'.format(self.headers['account']), headers=self.headers).json['account']
        image_key = acc['image']['key']

        # Upload new image (the same one but again :P)
        with open(PATH_TO_IMAGE, 'rb') as image:
            files = [('image', 'username', image.read())]
            self.app.post(
                '/accounts/upload_image/',
                upload_files=files,
                headers=self.headers
            )
        # Get the account again and make sure the image key has changed and also the previous one is removed
        acc = self.app.get('/accounts/{}'.format(self.headers['account']), headers=self.headers).json['account']
        assert acc['image']['key'] != image_key
        images_path = '{}/images'.format(TEST_STORAGE)
        # Assert the previous one is removed
        assert path.isfile('{}/image-{}.jpg'.format(images_path, image_key)) is False

        #  Remove profile image
        self.app.get('/accounts/remove_image', headers=self.headers)

        # Get the account again to make sure it doesn't have image
        acc = self.app.get('/accounts/{}'.format(self.headers['account']), headers=self.headers).json['account']
        assert acc['image'] is None
        with open(PATH_TO_INVALID_IMAGE, 'rb') as image:
            files = [('image', 'username', image.read())]
            self.app.post(
                '/accounts/upload_image/',
                upload_files=files,
                headers=self.headers,
                status=400
            )
            # With no files provided
            self.app.post(
                '/accounts/upload_image/',
                headers=self.headers,
                status=400
            )

        # Get the image just posted
        # FIXME: served by webserver test
        get_resp = self.app.get('/accounts/%s' % viewee_resp['id'], headers=self.headers).json['account']

        # Post a document (application/pdf)
        with open(PATH_TO_DOCUMENT, 'rb') as document:
            files = [('document', 'username', document.read())]
            self.app.post(
                '/accounts/upload_document/',
                upload_files=files,
                headers=self.headers
            )
        with open(PATH_TO_INVALID_DOCUMENT, 'rb') as image:
            files = [('document', 'username', image.read())]
            self.app.post(
                '/accounts/upload_document/',
                upload_files=files,
                headers=self.headers,
                status=400
            )
        with open(PATH_TO_LARGE_DOCUMENT, 'rb') as image:
            files = [('document', 'username', image.read())]
            self.app.post(
                '/accounts/upload_document/',
                upload_files=files,
                headers=self.headers,
                status=400
            )

            # With no files provided
            self.app.post(
                '/accounts/upload_document/',
                headers=self.headers,
                status=400
            )
        # FIXME: served by webserver test
        get_resp = self.app.get('/accounts/%s' % viewee_resp['id'], headers=self.headers).json['account']
        """BlackBox Testing"""

        # Typo in keys
        invalid_account = {
            'username': 'invalid',
            'passwosrd': 'invalid',
            'email_address': 'invalid@test.com',
            'bio': 'invalid tester'
        }
        self.app.post_json('/users/register', params=invalid_account, headers=self.headers, status=400)

        # Dict lacking one pair
        invalid_account2 = {
            'username': 'invalid',
            'password': 'invalid',
            'bio': 'invalid tester'
        }
        self.app.post_json('/users/register', params=invalid_account2, headers=self.headers, status=400)

        # Invalid Json format
        self.app.post_json('/users/register', params=[], headers=self.headers, status=400)

        # Trying to violate username and email uniqueness
        self.app.post_json('/users/register', params=viewer_account, headers=self.headers, status=400)

        # Get with invalid account_id
        self.app.get('/accounts/%s' % 'invalid', headers=self.headers, status=400)

    def tearDown(self):
        super(TestAccount, self).tearDown()
        import os
        os.system('rm -rf {}'.format(TEST_STORAGE))
