# -*- coding: utf-8 -*-
"""
Integration tests for the Question.

"""
from nose.tools import assert_equal

from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header, keep_keys


class TestPost(TestController):
    """
    Tests for the Post Controller.
    """
    def setUp(self):
        super(TestPost, self).setUp()

        # Registering an account
        account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.account_post_resp = self.app.post_json('/users/register', params=account, headers=make_auth_header()).json
        valid_login = {
            'username': 'test',
            'password': 'test'
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])


    application_under_test = 'main'

    def test_question(self):
        """Testing Post, Delete Question, Answer and Comment"""

        # Posting a valid question (tag is already added in the setup)
        valid_question = {
            'post_type': 'Question',
            'parent_id': None,
            'title': 'test',
            'description': 'testing',
            'tags': 'tag,',
        }
        post_resp = self.app.post_json('/posts/', params=valid_question, headers=self.headers).json

        # Get the question just posted
        get_resp = self.app.get('/posts/%s' % post_resp['id'], headers=self.headers).json['post']
        assert_equal(
            keep_keys(['description', 'parent_id', 'post_type', 'title'], get_resp),
            keep_keys(['description', 'parent_id', 'post_type', 'title'], valid_question)
        )
        assert_equal(type(get_resp['accounts']), dict)
        assert_equal(get_resp['votes'], [])
        assert_equal(get_resp['views'], [{'post_id': 1, 'account_id': 1, 'id': 1}])
        assert_equal(get_resp['tags'], [{'id': 1, 'name': 'tag'}])

        # Get the question again with the same credentials to make sure the view doesnt increase
        get_resp = self.app.get('/posts/%s' % post_resp['id'], headers=self.headers).json['post']
        assert_equal(get_resp['views'], [{'post_id': 1, 'account_id': 1, 'id': 1}])
        # Edit the question
        valid_put = {
            'id': post_resp['id'],
            'title': 'edited test'
        }
        self.app.put_json('/posts/', params=valid_put, headers=self.headers)
        put_get_resp = self.app.get('/posts/%s' % post_resp['id'], headers=self.headers).json['post']
        assert_equal(put_get_resp['title'], valid_put['title'])

        # Try to change something rather than title or description
        invalid_put = {
            'id': post_resp['id'],
            'reputation': 10000
        }
        self.app.put_json('/posts/', params=invalid_put, headers=self.headers, status=400)

        # Put on a post that doesnt exist
        invalid_put = {
            'id': 242423423423423,
            'title': 'hi'
        }
        self.app.put_json('/posts/', params=invalid_put, headers=self.headers, status=404)

        # Put with invalid params
        invalid_put = {
            'id': post_resp['id'],
            'titlse': 'hi'
        }
        self.app.put_json('/posts/', params=invalid_put, headers=self.headers, status=400)

        invalid_put = {
            'id': post_resp['id'],
            'title': 'hi',
            'hello': 'I am extra!'
        }
        self.app.put_json('/posts/', params=invalid_put, headers=self.headers, status=400)

        # Put with no params!
        invalid_put = {}
        self.app.put_json('/posts/', params=invalid_put, headers=self.headers, status=400)

        # post an answer for the question just posted
        valid_answer = {
            'post_type': 'Answer',
            'parent_id': get_resp['id'],
            'description': 'this is an answer',
        }
        self.app.post_json('/posts/', params=valid_answer, headers=self.headers)

        # get the answers of the question just posted
        answers = self.app.get('/posts/get_children/%s' % get_resp['id'], headers=self.headers).json['answers']
        assert_equal(len(answers), 1)
        assert_equal(
            keep_keys(['description', 'parent_id', 'post_type'], answers[0]),
            keep_keys(['description', 'parent_id', 'post_type'], valid_answer),
        )
        # Post a comment for the answer just posted
        valid_comment = {
            'post_type': 'Comment',
            'parent_id': answers[0]['id'],
            'description': 'this is a comment',
        }
        comment_post_resp = self.app.post_json('/posts/', params=valid_comment, headers=self.headers).json

        comments = self.app.get(
            '/posts/get_children/%s' % answers[0]['id'],
            headers=self.headers
        ).json['comments']
        assert_equal(len(comments), 1)
        assert_equal(
            keep_keys(['description', 'parent_id', 'post_type'], comments[0]),
            keep_keys(['description', 'parent_id', 'post_type'], valid_comment),
        )

        # Post answer for non-existing parent_id!
        invalid_answer = {
            'post_type': 'Answer',
            'parent_id': 345345355,
            'description': 'this is an answer',
        }
        self.app.post_json('/posts', params=invalid_answer, headers=self.headers, status=409)

        # # Get children with invalid parent_id
        # self.app.get('/posts/get_children/234234234', headers=make_auth_header(), status=400)

        # post an answer with unnecessary `title` key
        invalid_answer = {
            'title': "I don't need title",
            'post_type': 'Answer',
            'parent_id': get_resp['id'],
            'description': 'this is an answer',
        }

        self.app.post_json('/posts/', params=invalid_answer, headers=self.headers, status=400)
        # Delete the question just got
        self.app.delete('/posts/%s' % get_resp['id'], headers=self.headers)

        # Get the question just deleted
        self.app.get('/posts/%s' % get_resp['id'], headers=self.headers, status=404)

        # Testing get_questions
        # Posting 20 questions
        for i in range(20):
            valid_question['description'] = 'testing%s' % i
            self.app.post_json('/posts/', params=valid_question, headers=self.headers)
        get_questions_resp = self.app.get(
            '/posts/get_questions', params={"from": 0, "to": 20},
            headers=self.headers
        ).json

        assert_equal(len(get_questions_resp['questions']), 20)

        """Blackbox testing"""
        # Get with invalid question_id
        self.app.get('/posts/%s' % 'invalid_id', headers=self.headers, status=400)

        # Testing get_questions with invalid params
        self.app.get('/posts/get_questions', params={'invalid': 0, 'to': 20}, headers=self.headers, status=400)
        self.app.get(
            '/posts/get_questions',
            params={'from': 'zero', 'to': 'twenty'},
            headers=self.headers,
            status=400
        )

        # Question dict lacking pairs
        invalid_question = {
            'post_type': 'Question',
            'title': 'test',
            'tags': 'tag,',
        }
        self.app.post_json('/posts/', params=invalid_question, headers=self.headers, status=400)

        # Question lacking post_type
        invalid_question2 = {
            'title': 'test',
            'tags': 'tag,',
        }
        self.app.post_json('/posts/', params=invalid_question2, headers=self.headers, status=400)

        # Question with invalid key
        invalid_question2 = {
            'post_type': 'Question',
            'invalid_key': 'test',
            'description': 'testing',
            'tags': 'tag,',
        }
        self.app.post_json('/posts/', params=invalid_question2, headers=self.headers, status=400)

        # Trying to violate description uniqueness by posting valid_question again
        self.app.post_json('/posts/', params=valid_question, headers=self.headers, status=400)

        # Invalid json request
        self.app.post_json('/posts/', params=[], headers=self.headers, status=400)

        # Delete with invalid id
        self.app.delete('/posts/%s' % 'invalid_id', headers=self.headers, status=400)

        # Delete non-existing id
        self.app.delete('/posts/%s' % 234352453, headers=self.headers, status=404)

        # Invalid question with invalid post_type
        invalid_question3 = {
            'post_type': 'hello',
            'parent_id': None,
            'title': 'test',
            'description': 'testing',
            'tags': 'tag,',
        }
        self.app.post_json('/posts/', params=invalid_question3, headers=self.headers, status=400)

        # Get a question with non-existing id
        self.app.get('/posts/%s' % 2134324234, headers=self.headers, status=404)

        # Set a comment as parent_id!
        invalid_comment = {
            'post_type': 'Comment',
            'parent_id': comment_post_resp['id'],
            'description': 'anything'
        }
        self.app.post_json('/posts', params=invalid_comment, headers=self.headers, status=400)