# -*- coding: utf-8 -*-
"""
Integration tests for the Feed.

"""
from os import path

from nose.tools import assert_equal

import transaction
import rest_ave
from rest_ave import model
from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header, keep_keys

PATH_TO_IMAGE = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'image.jpg'))


class TestFeed(TestController):
    """
    Tests for the Feed Controller.
    """
    def setUp(self):
        super(TestFeed, self).setUp()

        # Registering an account
        account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.account_post_resp = self.app.post_json('/users/register', params=account, headers=make_auth_header()).json
        valid_login = {
            'username': 'test',
            'password': 'test'
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

        model.\
            DBSession.\
            query(model.Account).\
            filter(model.Account.id == self.headers['account']).\
            one().\
            is_official = True
        transaction.commit()
    application_under_test = 'main'

    def test_feed(self):
        """Testing Feed Controller"""
        # Posting a feed without image
        valid_feed = {
            'title': 'feed',
            'description': 'description feed',
            'image': None
        }
        resp = self.app.post('/feeds/', params=valid_feed, headers=self.headers).json

        # Get the feed just posted
        get_resp = self.app.get('/feeds/{}'.format(resp['id']), headers=self.headers).json['feed']
        assert_equal(keep_keys(valid_feed.keys(), get_resp), valid_feed)

        # Posting a feed with image
        valid_feed = {
            'title': 'feed',
            'description': 'description feed',
        }
        with open(PATH_TO_IMAGE, 'rb') as image:
            files = [('image', 'feed_image', image.read())]
            resp = self.app.post(
                '/feeds/',
                params=valid_feed,
                upload_files=files,
                headers=self.headers
            ).json
            get_resp = self.app.get('/feeds/{}'.format(resp['id']), headers=self.headers).json['feed']
            assert get_resp['image'] is not None

            # Post 20 feeds and get them all (22 ;))
            for i in range(20):
                self.app.post(
                    '/feeds/',
                    params=valid_feed,
                    upload_files=files,
                    headers=self.headers
                )
            get_resp = self.app.get('/feeds/get_feeds', headers=self.headers).json['feeds']
            assert len(get_resp) == 22

