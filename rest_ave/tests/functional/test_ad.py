# -*- coding: utf-8 -*-
"""
Integration tests for the Ad.

"""
from os import path

from nose.tools import assert_equal

import rest_ave
from rest_ave.tests import TestController
from rest_ave.tests.helpers import make_auth_header, keep_keys

PATH_TO_IMAGE = path.abspath(path.join(path.dirname(rest_ave.__file__), 'tests', 'stuff', 'image.jpg'))


class TestAd(TestController):
    """
    Tests for the Ad Controller.
    """
    def setUp(self):
        super(TestAd, self).setUp()

        # Registering an account
        self.account = {
            'username': 'test',
            'password': 'test',
            'email_address': 'test@test.com',
            'bio': 'tester'
        }
        self.account_post_resp = self.app.post_json('/users/register', params=self.account, headers=make_auth_header()).json

        valid_login = {
            'username': 'test',
            'password': 'test'
        }
        login_resp = self.app.post_json('/users/login', params=valid_login, headers=make_auth_header()).json
        self.headers = make_auth_header(account_id=login_resp['user']['id'], session_id=login_resp['session'])

    def test_ad(self):
        """Testing ads"""
        environ = {'REMOTE_USER': 'manager'}  # Make sure to add ads permission for manager
        # Ad without url
        valid_add = {
            'account_id': self.account_post_resp['id'],
            'priority': 0
        }
        with open(PATH_TO_IMAGE, 'rb') as image:
            files = [('image', 'ad_image', image.read())]
            self.app.post(
                '/submit_ad',
                params=valid_add,
                upload_files=files,
                extra_environ=environ,
            )

        # Get the ads
        ads = self.app.get('/ads', headers=self.headers).json['ads']
        assert len(ads) == 1
        assert ads[0]['accounts']['username'] == self.account['username']

        # Ad with url
        valid_add = {
            'account_id': self.account_post_resp['id'],
            'url': 'http://www.url.com',
            'priority': 0
        }
        with open(PATH_TO_IMAGE, 'rb') as image:
            files = [('image', 'ad_image', image.read())]
            self.app.post('/submit_ad', params=valid_add, upload_files=files, extra_environ=environ)

        # Get the ads
        ads = self.app.get('/ads', headers=self.headers).json['ads']
        assert len(ads) == 2
        assert ads[0]['url'] is None
        assert ads[1]['url'] == valid_add['url']
