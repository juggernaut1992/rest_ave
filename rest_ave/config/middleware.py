# -*- coding: utf-8 -*-
"""WSGI middleware initialization for the rest-ave application."""
import functools
from os import path

from tg import config
from sqlalchemy_media import StoreManager, FileSystemStore

import rest_ave
from rest_ave.config.app_cfg import base_config
from rest_ave.config.environment import load_environment


__all__ = ['make_app']


PUBLIC_PATH = path.abspath(path.join(path.dirname(rest_ave.__file__), 'public'))
# Use base_config to setup the necessary PasteDeploy application factory.
# make_base_app will wrap the TG2 app with all the middleware it needs.
make_base_app = base_config.setup_tg_wsgi_app(load_environment)


def make_app(global_conf, full_stack=True, **app_conf):
    """
    Set rest-ave up with the settings found in the PasteDeploy configuration
    file used.

    :param global_conf: The global settings for rest-ave (those
        defined under the ``[DEFAULT]`` section).
    :type global_conf: dict
    :param full_stack: Should the whole TG2 stack be set up?
    :type full_stack: str or bool
    :return: The rest-ave application with all the relevant middleware
        loaded.

    This is the PasteDeploy factory for the rest-ave application.

    ``app_conf`` contains all the application-specific settings (those defined
    under ``[app:main]``.
    """

    app = make_base_app(global_conf, full_stack=True, **app_conf)
    base_url = config.get('base_url')

    storage_path = config.get('storage_path')
    temp_path = '{}/{}'.format(PUBLIC_PATH, storage_path)

    # Wrap your base TurboGears 2 application with custom middleware here
    StoreManager.register(
        'fs',
        functools.partial(FileSystemStore, temp_path, base_url),
        default=True
    )
    return app
