<%inherit file="local:templates.master"/>
<%def name="title()">Ad Submission</%def>
  <h1>Submit ad</h1>

  <div class="row">
    <div class="col-md-8 col-md-offset-1">
      <form action="${tg.url('submit_ad')}" enctype="multipart/form-data"
            method="post" accept-charset="UTF-8" class="form-horizontal">
        <div class="form-group">
          <label class="col-sm-2 control-label">account_id:</label>
          <div class="col-sm-10">
            <input class="form-control" type="text" name="account_id"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">url:</label>
          <div class="col-sm-10">
            <input class="form-control" type="text" name="url"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">priority:</label>
          <div class="col-sm-10">
            <input class="form-control" type="text" name="priority"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">image:</label>
          <div class="col-sm-10">
            <input class="form-control" type="file" name="image"/>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-10 col-sm-offset-2">
            <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>