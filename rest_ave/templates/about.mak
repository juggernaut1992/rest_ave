<link href="/css/callback.css" rel="stylesheet">
<h1><span class="blue"></span>Community<span class="blue"></span></h1>

% if ok:
    <h2>Your Community Account is Now Official!</h2>
    <table class="container">
        <thead>
            <tr>
			<th><h1>Username</h1></th>
			<th><h1>Transaction ID</h1></th>
			<th><h1>Official</h1></th>
			<th><h1>Paid</h1></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>${username}</td>
                <td>${ref_id}</td>
                <td>Yes!</td>
                <td>30,000T</td>
            </tr>
        </tbody>
    </table>
% else:
    <h2>Something went wrong! Please try again or contact us for support</h2>
    <h2></h2>
% endif
