# -*- coding: utf-8 -*-
from datetime import datetime

from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import Integer, Unicode, Enum, DateTime

from rest_ave.model import DeclarativeBase


class Purchase(DeclarativeBase):

    __tablename__ = 'purchases'

    id = Column(Integer, primary_key=True)
    authority = Column(Unicode(255), nullable=True)
    ref_id = Column(Unicode(255), nullable=True)
    status = Column(Enum('Pending', 'Done', 'Failed', name='status'), default='Pending')

    account_id = Column(Integer, ForeignKey(
        'accounts.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)

    created = Column(DateTime, default=datetime.now)
