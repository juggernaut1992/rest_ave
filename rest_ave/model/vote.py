# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey
from sqlalchemy.sql.schema import UniqueConstraint
from sqlalchemy.types import Integer, Enum
from sqlalchemy.orm import relationship, backref

from rest_ave.model import DeclarativeBase


class Vote(DeclarativeBase):

    """
    Created on Oct, 14, 2016

    Vote for Question, Answer and Comment
    """

    __tablename__ = 'votes'

    id = Column(Integer, primary_key=True)
    value = Column(Enum('Up', 'Down', name='vote_value'))

    account_id = Column(Integer, ForeignKey(
        'accounts.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    post_id = Column(Integer, ForeignKey(
        'posts.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)

    __table_args__ = (UniqueConstraint('account_id', 'post_id', 'value'), )
