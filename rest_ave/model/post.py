# -*- coding: utf-8 -*-

from datetime import datetime

from sqlalchemy.sql.schema import UniqueConstraint
from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy.types import Integer, Unicode, DateTime, Enum
from sqlalchemy.orm import relationship, backref

from rest_ave.model import DeclarativeBase, View


association_table = Table(
    'post_tag',
    DeclarativeBase.metadata,
    Column('post_id', Integer, ForeignKey('posts.id')),
    Column('tag_id', Integer, ForeignKey('tags.id'))
)


class Post(DeclarativeBase):

    """
    Created on Oct, 14, 2016

    Model for Question, Answer and Comment
    Having title, votes, views and tags as nullable fields as a workaround for handling all three Post types
    in a single class.
    parent_id is used for accessing answers and comments of a question, thus null if post_type == 1 (question)

    """

    __tablename__ = 'posts'

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, nullable=True)  # null if post type is question

    post_type = Column(Enum('Question', 'Answer', 'Comment', name='post_types'))

    title = Column(Unicode(255), nullable=True)  # Null if post type is answer or comment
    description = Column(Unicode(1000), nullable=False, unique=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    edit_date = Column(DateTime, nullable=True)
    report_count = Column(Integer, nullable=False, default=0)
    account_id = Column(Integer, ForeignKey(
        'accounts.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    views = relationship('View', backref=backref("posts"), cascade="all, delete-orphan")
    votes = relationship('Vote', backref=backref('posts'), cascade="all, delete-orphan")

    tags = relationship(
        "Tag",
        secondary=association_table,
        back_populates="posts")
    __table_args__ = (UniqueConstraint('account_id', 'description'), )

    def append_view(self, view):
        if isinstance(view, View):
            self.views.append(view)


class Tag(DeclarativeBase):
    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255), unique=True)

    posts = relationship(
        "Post",
        secondary=association_table,
        back_populates="tags")

