# -*- coding: utf-8 -*-

import os
from datetime import datetime
from hashlib import sha256
import json
from base64 import b64encode

from tg import config, session
from redis import StrictRedis
from sqlalchemy import Column, Table, ForeignKey, TypeDecorator, func, select
from sqlalchemy.sql.schema import UniqueConstraint
from sqlalchemy.types import Integer, Unicode, DateTime, Enum, Boolean
from sqlalchemy.orm import synonym, relationship, backref, column_property
from sqlalchemy.event import listen
from sqlalchemy_media import Image, WandAnalyzer, ImageValidator, ImageProcessor, File, MagicAnalyzer, \
    ContentTypeValidator
from sqlalchemy_media.constants import MB, KB
from sqlalchemy_media.attachments import AttachmentList

from rest_ave import model
from rest_ave.lib.helpers import random_string


profile_view = Table(
    'profile_views', model.DeclarativeBase.metadata,
    Column('viewer_id', Integer, ForeignKey('accounts.id'), index=True),
    Column('viewee_id', Integer, ForeignKey('accounts.id')),
    UniqueConstraint('viewer_id', 'viewee_id', name='unique_viewer')
)


class OfficialDocument(File):
    __pre_processors__ = [
        MagicAnalyzer(),
        ContentTypeValidator(['application/pdf', 'image/jpeg'])
    ]
    __max_length__ = 512*KB
    __min_length__ = 10*KB


class OfficialDocuments(AttachmentList):
    __item_type__ = OfficialDocument


class Json(TypeDecorator):
    impl = Unicode

    def process_bind_param(self, value, engine):
        return json.dumps(value)

    def process_result_value(self, value, engine):
        if value is None:
            return None

        return json.loads(value)


class Picture(Image):
    __pre_processors__ = [
        WandAnalyzer(),
        ImageValidator(
            minimum=(512, 386),
            maximum=(3264, 2448),
            content_types=['image/jpeg', 'image/png']
        ),
        ImageProcessor(
            fmt='jpeg',
            crop=dict(
                width=512,
                height=386,
                gravity='center'
            )
        )
    ]


class Account(model.DeclarativeBase):

    """
    Created on Oct, 14, 2016

    User accounts

    """

    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True)
    username = Column(Unicode(25), unique=True, nullable=False)
    _password = Column('password', Unicode(128))
    email_address = Column(Unicode(50), unique=True, nullable=False)
    bio = Column(Unicode(100), nullable=True)
    created = Column(DateTime, default=datetime.now)
    image = Column(Picture.as_mutable(Json), nullable=True)
    reputation = Column(Integer, default=0)
    is_official = Column(Boolean, default=False)
    official_start = Column(DateTime, nullable=True)
    official_documents = Column(OfficialDocuments.as_mutable(Json), nullable=True)
    viewers = relationship(
        'Account',
        secondary=profile_view,
        primaryjoin=id == profile_view.c.viewer_id,
        secondaryjoin=id == profile_view.c.viewee_id
    )
    notifications = relationship('Notification', backref=backref('accounts'), cascade="all, delete-orphan")
    posts = relationship('Post', backref=backref('accounts'), cascade="all, delete-orphan")
    ads = relationship('Ad', backref=backref('accounts'), cascade="all, delete-orphan")
    feeds = relationship('Feed', backref=backref('accounts'), cascade="all, delete-orphan")
    views = relationship('View', backref=backref('accounts'), cascade="all, delete-orphan")
    purchases = relationship('Purchase', backref=backref('accounts'), cascade="all, delete-orphan")
    votes = relationship('Vote', backref=backref('accounts'), cascade="all, delete-orphan")
    deletions = relationship('AccountDeletion', backref=backref('accounts'), cascade="all, delete-orphan")

    @classmethod
    def _hash_password(cls, password):
        salt = sha256()
        salt.update(os.urandom(60))
        salt = salt.hexdigest()
        hash = sha256()
        hash.update((password + salt).encode('utf-8'))
        hash = hash.hexdigest()
        return salt + hash

    def _set_password(self, password):
        self._password = self._hash_password(password)

    def _get_password(self):
        return self._password

    password = synonym('_password', descriptor=property(_get_password,
                                                        _set_password))

    def validate_password(self, password):
        hash = sha256()
        hash.update((password + self.password[:64]).encode('utf-8'))
        return self.password[64:] == hash.hexdigest()

    def session_token(self):
        _session = random_string(32)
        conn = StrictRedis(db=config.get('redis_session_db'))
        conn.set(self.id, _session)
        return _session

    def wipe_session(self):
        conn = StrictRedis(db=config.get('redis_session_db'))
        conn.delete(self.id) if conn.get(self.id) else None

    def append_viewer(self, viewer):
        if isinstance(viewer, Account) and viewer not in self.viewers:
            self.viewers.append(viewer)

    def store_notification(self, actor_name, action, post_id):
        n = model.Notification(
            action=action,
            actor=actor_name,
            post_id=post_id,
            account_id=self.id
        )
        n.set_text()
        self.notifications.append(n)

        # Store in redis
        conn = StrictRedis(db=config.get('redis_notif_db'))
        conn.set(self.id, n.text)


def on_update(mapper, connection, target):
    """Check for possible badge award"""
    pass

listen(Account, 'after_update', on_update)


class Feed(model.DeclarativeBase):

    __tablename__ = 'feeds'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(255), nullable=False)
    description = Column(Unicode(1000), nullable=False)
    image = Column(Picture.as_mutable(Json), nullable=True)
    created = Column(DateTime, default=datetime.now)

    account_id = Column(
        Integer, ForeignKey(
            'accounts.id', onupdate='CASCADE', ondelete='CASCADE'
        ),
        nullable=False
    )


class Ad(model.DeclarativeBase):

    __tablename__ = 'ads'

    id = Column(Integer, primary_key=True)
    url = Column(Unicode(255), nullable=True)
    priority = Column(Integer, default=1)
    image = Column(Picture.as_mutable(Json), nullable=False)

    account_id = Column(
        Integer, ForeignKey(
            'accounts.id', onupdate='CASCADE', ondelete='CASCADE'
        ),
        nullable=False
    )


class AccountDeletion(model.DeclarativeBase):
    __tablename__ = 'deletions'

    id = Column(Integer, primary_key=True)
    token = Column(Unicode(255), nullable=False)
    created = Column(DateTime, default=datetime.now)
    account_id = Column(Integer, ForeignKey(
        'accounts.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
