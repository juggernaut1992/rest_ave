# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey
from sqlalchemy.sql.schema import UniqueConstraint
from sqlalchemy.types import Integer, Unicode, Enum
from sqlalchemy.orm import relationship, backref, synonym

from rest_ave.model import DeclarativeBase


class Notification(DeclarativeBase):
    __tablename__ = 'notifications'

    id = Column(Integer, primary_key=True)
    action = Column(Enum('Comment', 'Answer', name='actions'))
    actor = Column(Unicode(255), nullable=False)
    text = Column(Unicode(255))

    def set_text(self):
        self.text = '{} left {} {} for you'.format(self.actor, 'an' if self.action == 'Answer' else 'a', self.action)

    def get_text(self):
        return self.text

    account_id = Column(Integer, ForeignKey(
        'accounts.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)  # Owner

    post_id = Column(Integer, ForeignKey(
        'posts.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
