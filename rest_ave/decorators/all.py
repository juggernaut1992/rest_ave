from base64 import b64decode
from binascii import Error as DecodeError
from json.decoder import JSONDecodeError

from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError
from sqlalchemy_media.stores import StoreManager
from tg import request, abort, config, session
from pyDes import triple_des
from redis import StrictRedis

from rest_ave.model import DBSession, Account, Post, View
from rest_ave.lib.helpers import represents_int


def authorize(f):
    def wrapped(*args, **kwargs):
        secret_key = config.get('auth_secret_key')
        auth_message = config.get('auth_message')
        token = request.environ.get('HTTP_TOKEN')
        if not token:
            abort(401, detail='Authentication failed', passthrough='json')
        try:
            gibberish = b64decode(token)
            if len(gibberish) % 8 != 0:
                raise ValueError
        except (DecodeError, ValueError):
            abort(401, detail='Authentication failed', passthrough='json')
        if triple_des(secret_key).decrypt(gibberish, padmode=2).decode() != auth_message:
            abort(401, detail='Authentication failed', passthrough='json')
        # Escape session authentication if user wants to login or register (test_auth for test purposes)
        path = request.path
        if path not in ('/users/register', '/users/login', '/test_auth'):
            account_id = int(request.environ.get('HTTP_ACCOUNT')) \
                if represents_int(request.environ.get('HTTP_ACCOUNT')) \
                else 0
            session_id = request.environ.get('HTTP_SESSION')
            if not account_id or not session_id:
                abort(403, detail='Invalid Session info', passthrough='json')
            conn = StrictRedis(db=config.get('redis_session_db'))
            result = conn.get(account_id)
            if not result:
                abort(403, detail='Invalid Session info', passthrough='json')
            if result.decode() != session_id:
                abort(403, detail='Invalid Session info', passthrough='json')
        return f(*args, **kwargs)
    return wrapped


def increment_post_view(f):
    def wrapped(*args, **kwargs):
        try:
            account_id = int(request.environ['HTTP_ACCOUNT'])
            post_id = int(args[1])
        except (ValueError, KeyError):
            abort(400, detail='account_id and post_id must be provided and be int', passthrough='json')
        # Make sure post exists
        post = DBSession.query(Post).filter(Post.id == post_id).one_or_none()
        if not post:
            abort(404, detail='Post not found!', passthrough='json')
        if post.post_type == 'Question':
            view = View(account_id=account_id, post_id=post_id)
            try:
                DBSession.add(view)
                DBSession.flush()
            except IntegrityError:
                DBSession.rollback()
        return f(*args, **kwargs)
    return wrapped


def increment_account_view(f):
    def wrapped(*args, **kwargs):
        viewer_id = int(request.environ['HTTP_ACCOUNT'])
        if not represents_int(args[1]):
            abort(400, detail='account_id must be int', passthrough='json')
        with StoreManager(DBSession):
            viewee = DBSession.query(Account).filter(Account.id == args[1]).one_or_none()
            viewer = DBSession.query(Account).filter(Account.id == viewer_id).one_or_none()
            if viewee and viewer is not viewee:
                viewee.append_viewer(viewer)
                try:
                    DBSession.flush()
                except IntegrityError:
                    DBSession.rollback()
        return f(*args, **kwargs)
    return wrapped


def ensure_put_validity(f):
    def wrapped(*args, **kwargs):
        params = request.json
        allowed_items = ['title', 'description', 'tags', 'id']
        [
            abort(400, detail='You can only put on title, description and/or tags', passthrough='json')
            for k, v in params.items() if k.lower() not in allowed_items
        ]
        return f(*args, **kwargs)
    return wrapped


def ensure_json_request(f):
    def wrapped(*args, **kwargs):
        try:
            params = request.json
            if not isinstance(params, dict):
                raise ValueError
            kwargs = params
        except (JSONDecodeError, ValueError):
            abort(400, detail='Only application/json requests are allowed', passthrough='json')
        return f(*args, **kwargs)
    return wrapped


def ensure_post_required_params_exist(f):
    def wrapped(*args, **kwargs):
        required_params = ['post_type', 'description', 'parent_id']
        post_type = kwargs.get('post_type')
        if not post_type:
            abort(400, detail='post_type must be provided', passthrough='json')
        if post_type == 'Question':
            required_params.append('title')
            required_params.append('tags')
        if sorted(list(kwargs.keys())) != sorted(required_params):
            abort(400, detail='Required keys are not provided', passthrough='json')
        return f(*args, **kwargs)
    return wrapped


def impose_restrictions(f):
    """Impose restrictions against non-official accounts"""
    def wrapped(*args, **kwargs):
        account_id = int(request.environ.get('HTTP_ACCOUNT'))
        if DBSession.query(Account).filter(Account.id == account_id).one().is_official is False:
            abort(403, detail='Official accounts allowed only!', passthrough='json')
        return f(*args, **kwargs)
    return wrapped
