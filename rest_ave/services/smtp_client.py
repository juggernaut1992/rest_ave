# -*- coding: utf-8 -*-
import smtplib
import threading
from email.mime.text import MIMEText

from tg import config as tg_config


class SMTPClient(object):
    def __init__(self, name, email, token):
        self.name = name

        self.mail_host = tg_config.get('mail.smtp_server.host')
        self.mail_port = tg_config.get('mail.smtp_server.port')
        self.username = tg_config.get('mail.smtp_username')
        self.password = tg_config.get('mail.smtp_password')
        self.mail_sender = tg_config.get('mail.from_address')
        self.mail_receiver = email
        self.callback = tg_config.get('base_url')
        self.body = '''
        <div style="margin-bottom: 5px;">
            <p style="text-align: center;">Hey, Are you sure you want to remove your Community account? This action is unrecovarable!</p>
            <a href="{}users/account_deletion_callback?token={}" style="margin-left: 50%%; margin-top:5px; -webkit-appearance: button; -moz-appearance: button; appearance: button; border-radius:5px;">Confirm Deletion>
        </a></div>
        '''.format(self.callback, token)
        self.smtp_client = smtplib.SMTP(self.mail_host, self.mail_port)
        self.smtp_client.ehlo_or_helo_if_needed()
        self.smtp_client.starttls()
        self.smtp_client.login(self.username, self.password)

    def make_mail(self):
        message = MIMEText(self.body, _charset='utf-8')
        message['From'] = self.mail_sender
        message['To'] = self.mail_receiver
        message['Subject'] = "Delete Community Account"
        return message.as_string()

    def send_mail(self):
        t = threading.Thread(target=self._send_mail)
        t.daemon = True
        t.start()

    def _send_mail(self):
        self.smtp_client.sendmail(self.mail_sender, self.mail_receiver, self.make_mail())
        self.smtp_client.quit()
