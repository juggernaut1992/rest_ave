from datetime import datetime
import json

import psycopg2


class Detective:
    def __init__(self, config):
        self.config = config
        self.host = self.config.get('host')
        self.dbname = self.config.get('db_name')
        self.user = self.config.get('user')
        self.password = self.config.get('password')
        self.conn_string = "host='{}' dbname='{}' user='{}' password='{}'".format(
            self.host,
            self.dbname,
            self.user,
            self.password
        )
        self.conn = psycopg2.connect(self.conn_string)
        self.cursor = self.conn.cursor()
        self.interests = []
        self.detect()

    def detect(self):
        now = datetime.now()
        self.cursor.execute(
            """
                SELECT * FROM accounts
                WHERE is_official = 'True'
                AND DATE_PART('day', (%s)::TIMESTAMP - official_start::TIMESTAMP) > '30'
            """, (now, ))
        self.interests = self.cursor.fetchall()
        self._purge()

    def _purge(self):
        if not self.interests:
            return
        wheres = [' WHERE ']
        for a in self.interests:
            wheres.append(
                'id = {}'.format(a[0] if a == self.interests[-1] else '{} OR '.format(a[0]))
            )
        query = """
        UPDATE accounts
        SET is_official = False, official_start = NULL
        %s;
        """ % ''.join(wheres)
        self.cursor.execute(query)
        self.conn.commit()


if __name__ == '__main__':
    with open('detective_config.json', 'r') as conf:
        config = json.load(conf)
    d = Detective(config)
    d.detect()
