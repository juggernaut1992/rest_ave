# -*- coding: utf-8 -*-
"""Main Controller"""
from urllib.error import URLError
from datetime import datetime

import transaction
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import joinedload
from sqlalchemy_media.exceptions import DimensionValidationError, ContentTypeValidationError
from sqlalchemy_media.stores import StoreManager
from tg import expose, flash, config, lurl, abort, require, request, redirect, tmpl_context, predicates, cached
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController

from rest_ave import model
from rest_ave.controllers.post import PostController
from rest_ave.controllers.account import AccountController
from rest_ave.controllers.user import UserController
from rest_ave.controllers.feed import FeedController
from rest_ave.model import DBSession, Purchase, Account
from rest_ave.lib.base import BaseController
from rest_ave.controllers.error import ErrorController
from rest_ave.lib.helpers import represents_int, represents_bool, is_image
from rest_ave.decorators.all import ensure_json_request, authorize
from rest_ave.services.zarinpal import ZarinpalClient

__all__ = ['RootController']


class RootController(BaseController):

    communityarea = AdminController(model, DBSession, config_type=TGAdminConfig)
    posts = PostController()
    accounts = AccountController()
    users = UserController()
    error = ErrorController()
    feeds = FeedController()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "rest_ave"

    @expose('rest_ave.templates.index')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('json')
    @authorize
    def about_us(self):
        name = config.get('app.name')
        description = config.get('app.description')
        author = config.get('app.author')
        certificate = config.get('app.certificate')
        return dict(name=name, description=description, author=author, certificate=certificate)

    @expose()
    @authorize
    def test_auth(self):
        pass

    @expose('json')
    @authorize
    def ranking(self, _from, to):
        # Warning: Not tested due to official account being bound to purchase
        if not represents_int(_from) or not represents_int(to):
            abort(400, detail='_from, to must represent int', passthrough='json')
        ranking = DBSession. \
            query(Account). \
            filter(Account.is_official == True). \
            order_by(Account.reputation)[int(_from):int(to)]
        return dict(ranking=ranking)

    @expose('json')
    @authorize
    def tags(self):
        """Returns all tags"""
        tags = DBSession.query(model.Tag).all()
        return dict(tags=tags)

    @expose('json')
    @authorize
    def ads(self):
        ads = DBSession.query(model.Ad).options(joinedload('accounts')).all()
        return dict(ads=ads)

    @expose('rest_ave.templates.ad')
    @require(predicate=predicates.has_permission('ads'))
    def ad(self):
        return dict()

    @expose('json')
    @require(predicate=predicates.has_permission('ads'))
    def submit_ad(self, **kwargs):
        url = kwargs.get('url')
        account_id = kwargs.get('account_id')
        image = kwargs.get('image')
        priority = kwargs.get('priority')
        if not represents_int(account_id) or not represents_int(priority) or not is_image(image):
            abort(400, detail='required params not provided', passthrough='json')
        ad = model.Ad(
            url=url,
            account_id=int(account_id),
            priority=priority
        )
        DBSession.add(ad)
        try:
            with StoreManager(DBSession):
                ad.image = model.Picture.create_from(image.file)
        except (DimensionValidationError, ContentTypeValidationError) as ex:
            DBSession.rollback()
            abort(400, detail=str(ex), passthrough='json')
        try:
            DBSession.flush()
        except IntegrityError as ex:
            DBSession.rollback()
            abort(409, detail=str(ex), passthrough='json')
        return dict(OK=True)

    @expose('json')
    @authorize
    def authenticate_ws_client(self, **kwargs):
        """Authenticate pynotif requests"""
        return dict(ok=True)

    @expose('rest_ave.templates.purchase_callback')
    def purchase_callback(self, **kwargs):
        authority = kwargs.get('Authority')
        status = kwargs.get('Status')
        if status == 'NOK':
            return dict(ok=False)
        elif status == 'OK':
            try:
                zp = ZarinpalClient()
                ref_id, status = zp.verify_payment(authority)
            except URLError:
                abort(409, detail='ZarinPal Temporary Down', passthrough='json')
            if ref_id:
                purchase = DBSession.\
                    query(Purchase).\
                    filter(Purchase.authority == authority).\
                    options(joinedload('accounts')).\
                    one_or_none()
                if purchase:
                    purchase.ref_id = ref_id
                    purchase.status = 'Done'
                    purchase.accounts.is_official = True
                    purchase.accounts.official_start = datetime.now()
                    return dict(ok=True, ref_id=ref_id, username=purchase.accounts.username)
                else:
                    return dict(ok=False)
            else:
                return dict(ok=False)

    @expose()
    def make_purchase(self, **kwargs):
        account_id = kwargs.get('account_id')
        email = kwargs.get('email')
        mobile = kwargs.get('mobile')
        try:
            zp = ZarinpalClient(email=email, mobile=mobile)
            authority = zp.make()
        except URLError as ex:
            abort(409, detail='ZarinPal Temporary Down', passthrough='json')
        if authority:
            payment_url = config.get('payment')
            purchase = Purchase(account_id=account_id, authority=authority)
            DBSession.add(purchase)
            try:
                DBSession.flush()
                redirect('{}/{}'.format(payment_url, authority))
            except IntegrityError:
                pass
        else:
            abort(400)

    @expose('json')
    @authorize
    def search(self, query, _for):
        if _for == 'account':
            result = DBSession.query(model.Account).filter(model.Account.username.contains(query)).all()
        elif _for == 'post':
            result = DBSession.query(model.Post).filter(model.Post.title.contains(query)).all()
            if not result:
                result = DBSession.query(model.Post).filter(model.Post.description.contains(query)).all()
        else:
            abort(400, detail='either look for account or post!', passthrough='json')
        return dict(result=result)

    @expose('rest_ave.templates.login')
    def login(self, came_from=lurl('/'), failure=None, login=''):
        """Start the user login."""
        if failure is not None:
            if failure == 'user-not-found':
                flash(_('User not found'), 'error')
            elif failure == 'invalid-password':
                flash(_('Invalid Password'), 'error')

        login_counter = request.environ.get('repoze.who.logins', 0)
        if failure is None and login_counter > 0:
            flash(_('Wrong credentials'), 'warning')

        return dict(page='login', login_counter=str(login_counter),
                    came_from=came_from, login=login)

    @expose()
    def post_login(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on successful
        authentication or redirect her back to the login page if login failed.

        """
        if not request.identity:
            login_counter = request.environ.get('repoze.who.logins', 0) + 1
            redirect('/login',
                     params=dict(came_from=came_from, __logins=login_counter))
        userid = request.identity['repoze.who.userid']
        flash(_('Welcome back, %s!') % userid)

        # Do not use tg.redirect with tg.url as it will add the mountpoint
        # of the application twice.
        return HTTPFound(location=came_from)

    @expose()
    def post_logout(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on logout and say
        goodbye as well.

        """
        flash(_('We hope to see you soon!'))
        return HTTPFound(location=came_from)
