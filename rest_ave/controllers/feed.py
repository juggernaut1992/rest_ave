# -*- coding: utf-8 -*-
"""Account controller module"""
from cgi import FieldStorage
from json.decoder import JSONDecodeError

import transaction
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import joinedload
from sqlalchemy_media.exceptions import DimensionValidationError
from sqlalchemy_media.stores import StoreManager, FileSystemStore
from tg import expose, abort, request
from tg.controllers.restcontroller import RestController

from rest_ave.model import DBSession, Account, Feed, Picture
from rest_ave.model.account import OfficialDocument, OfficialDocuments
from rest_ave.decorators import authorize, impose_restrictions
from rest_ave.lib.helpers import represents_int, is_image


class FeedController(RestController):

    @expose('json')
    @authorize
    def get_one(self, feed_id):
        with StoreManager(DBSession):
            feed = DBSession.query(Feed).\
                filter(Feed.id == feed_id).\
                options(joinedload('accounts')). \
                one_or_none()
        if not feed:
            abort(status_code=404, detail='No such feed', passthrough='json')
        return dict(feed=feed)

    @expose('json')
    @authorize
    @impose_restrictions
    def post(self, **kwargs):
        required_params = ['title', 'description', 'image']
        if sorted(list(kwargs.keys())) != sorted(required_params):
            abort(400, detail='Required keys are not provided', passthrough='json')
        image = kwargs.pop('image')
        feed = Feed()
        for k, v in kwargs.items():
            setattr(feed, k, v)
        feed.account_id = int(request.environ.get('HTTP_ACCOUNT'))
        DBSession.add(feed)
        try:
            DBSession.flush()
            # Add image if provided
            if is_image(image):
                try:
                    with StoreManager(DBSession):
                        feed.image = Picture.create_from(image.file)
                        DBSession.flush()
                    return dict(id=feed.id)
                except DimensionValidationError as ex:
                    abort(400, detail=str(ex), passthrough='json')
            return dict(id=feed.id)
        except IntegrityError:
            abort(400, detail='Feed already exists', passthrough='json')

    @expose('json')
    @authorize
    def get_feeds(self):
        feeds = DBSession.query(Feed).all()
        return dict(feeds=feeds)
