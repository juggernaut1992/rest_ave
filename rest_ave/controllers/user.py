# -*- coding: utf-8 -*-
"""UserInteractions controller"""
from smtplib import SMTPException

from sqlalchemy.orm import joinedload
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from tg import abort, expose, request, flash, lurl
from tg.exceptions import HTTPFound
from tg.controllers.restcontroller import RestController

from rest_ave.model import DBSession, Account, AccountDeletion
from rest_ave.decorators import authorize, ensure_json_request
from rest_ave.services.smtp_client import SMTPClient
from rest_ave.lib.helpers import random_string


__all__ = ['UserController']


class UserController(RestController):

    @expose('json')
    @authorize
    @ensure_json_request
    def login(self, **kwargs):
        if sorted(list(kwargs.keys())) != sorted(['username', 'password']):
            abort(status_code=400, detail='required keys are not provided', passthrough='json')
        username = kwargs['username']
        password = kwargs['password']
        try:
            user = DBSession.query(Account).filter(Account.username == username).one()
        except NoResultFound:
            abort(status_code=404, detail='Invalid username or password', passthrough='json')
        if not user.validate_password(password):
            abort(status_code=401, detail='Invalid password', passthrough='json')
        user.session_token()
        return dict(user=user, session=user.session_token())

    @expose('json')
    @authorize
    def logout(self):
        user = DBSession.\
            query(Account).\
            filter(Account.id == int(request.environ.get('HTTP_ACCOUNT'))).\
            one_or_none()
        if not user:
            abort(409, detail='User not found!', passthrough='json')
        user.wipe_session()

    @expose('json')
    @authorize
    @ensure_json_request
    def register(self, **kwargs):
        account = Account()
        if sorted(list(kwargs.keys())) != sorted(['username', 'password', 'email_address', 'bio']):
            abort(status_code=400, detail='required keys are not provided', passthrough='json')
        for k, v in kwargs.items():
            setattr(account, k, v)
        DBSession.add(account)
        try:
            DBSession.flush()
        except IntegrityError:
            abort(status_code=400, detail='Username or email address is already taken', passthrough='json')
        return dict(id=account.id, ok=True)

    @expose('json')
    @authorize
    def notifications(self, account_id):
        account = DBSession.\
            query(Account).\
            filter(Account.id == account_id).\
            options(joinedload('notifications')).\
            one_or_none()
        if not account:
            abort(400, detail='no such account', passthrough='json')
        return dict(notifications=account.notifications)

    @expose('json')
    @authorize
    def require_authentication(self, **kwargs):
        # This is a method for testing authorize decorator
        return dict(OK=True)

    @expose('json')
    @authorize
    def delete_account(self):
        account_id = int(request.environ['HTTP_ACCOUNT'])
        account = DBSession.\
            query(Account).\
            filter(Account.id == account_id).\
            one()
        random_str = random_string(16)
        deletion = AccountDeletion(account_id=account_id, token=random_str)
        DBSession.add(deletion)
        DBSession.flush()
        try:
            client = SMTPClient(name=account.username, email=account.email_address, token=deletion.token)
            client.send_mail()
            return dict(detail='Check your email for confirmation!')
        except SMTPException:
            abort(409, detail='Something went wrong! try again later or contact us for support!', passthrough='json')

    @expose()
    def account_deletion_callback(self, token):
        deletion = DBSession.\
            query(AccountDeletion).\
            filter(AccountDeletion.token == token).\
            options(joinedload('accounts')). \
            one_or_none()
        if deletion:
            DBSession.delete(deletion.accounts)
            DBSession.flush()
            flash('Account Deleted!')
            return HTTPFound(location=lurl('/'))
        else:
            flash('Invalid Request!')
            return HTTPFound(location=lurl('/'))
