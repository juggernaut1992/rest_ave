# -*- coding: utf-8 -*-
"""Question controller module"""
from sqlalchemy import desc
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import joinedload
from tg import expose, abort, request, config
from tg.controllers.restcontroller import RestController

from rest_ave.model import DBSession, Post, View, Account, Vote
from rest_ave.decorators import *
from rest_ave.lib.helpers import represents_int, valid_tags


class PostController(RestController):

    @expose('json')
    @authorize
    @increment_post_view
    def get_one(self, post_id):
        # Used for getting questions, so we provide the comments too
        _id = int(post_id)
        post = DBSession.query(Post). \
            options(joinedload('accounts')). \
            options(joinedload('votes')). \
            options(joinedload('views')). \
            options(joinedload('tags')). \
            filter(Post.id == _id). \
            one_or_none()
        if post:
            comments = DBSession.query(Post). \
                options(joinedload('accounts')). \
                filter(Post.parent_id == post.id). \
                filter(Post.post_type == 'Comment').all()
            return dict(post=post, comments=comments)
        else:
            abort(404, detail='No such post!', passthrough='json')

    @expose('json')
    @authorize
    def get_children(self, post_id):
        if not represents_int(post_id):
            abort(400, detail='post_id must be int', passthrough='json')
        children = DBSession.query(Post).options(joinedload('accounts')).filter(Post.parent_id == post_id)
        answers = children.filter(Post.post_type == 'Answer').all()
        comments = children.filter(Post.post_type == 'Comment').all()
        return dict(answers=answers, comments=comments)

    @expose('json')
    @authorize
    def get_questions(self, **kwargs):
        if sorted(list(kwargs.keys())) != sorted(['from', 'to']):
            abort(status_code=400, detail='required keys are not provided', passthrough='json')
        try:
            _from = int(kwargs['from'])
            to = int(kwargs['to'])
        except ValueError:
            abort(status_code=400, detail='`from` and `to` must be int', passthrough='json')
        questions = DBSession.query(Post). \
            filter(Post.post_type == 'Question'). \
            options(joinedload('accounts')). \
            options(joinedload('tags')). \
            options(joinedload('votes')). \
            options(joinedload('views')). \
            order_by(desc(Post.creation_date))[_from:to]

        return dict(questions=questions)

    @expose('json')
    @authorize
    @ensure_json_request
    @ensure_post_required_params_exist
    def post(self, **kwargs):
        account_id = int(request.environ.get('HTTP_ACCOUNT'))
        post_type = kwargs.get('post_type')
        parent_post = DBSession. \
            query(Post). \
            filter(Post.id == kwargs.get('parent_id')). \
            options(joinedload('accounts')). \
            one_or_none()
        if parent_post and parent_post.post_type == 'Comment':
            abort(400, detail='Comment can\'nt have children', passthrough='json')
        if post_type in ('Answer', 'Comment') and not parent_post:
            abort(409, detail="Parent doesn't exist", passthrough='json')
        post_owner_account = parent_post.accounts if parent_post else None
        # Impose restriction
        if post_type == 'Comment':
            account = DBSession.query(Account).filter(Account.id == account_id).one()
            if account.reputation < int(config.get('min_com_rep')) and post_owner_account.id != account_id:
                abort(403, detail='You must have at least {} reputation to be able to comment!')
        post = Post()
        post.account_id = account_id
        if post_type == 'Question':
            tags = kwargs['tags'].split(',')
            for v in valid_tags(tags):
                post.tags.append(v)
            del kwargs['tags']
        if post_type != 'Question' and 'title' in kwargs.keys():
            abort(400, detail='Answer and Comment shall not have title!', passthrough='json')
        for k, v in kwargs.items():
            setattr(post, k, v)
        DBSession.add(post)
        try:
            DBSession.flush()
            # Store notification
            if post_type != 'Question':
                if post_owner_account.id == account_id:  # No notification for your own actions !
                    return dict(id=post.id)
                post_owner_account.store_notification(
                    action=post_type,
                    post_id=kwargs.get('parent_id'),
                    actor_name=DBSession.query(Account).filter(Account.id == account_id).one().username
                )
            return dict(id=post.id)
        except IntegrityError:
            abort(400, detail='Post already exists', passthrough='json')

    @expose('json')
    @authorize
    @ensure_put_validity
    def put(self):
        params = request.json
        _id = params.get('id') if 'id' in params.keys() else abort(400, detail='id not provided', passthrough='json')
        del params['id']
        post = DBSession.query(Post).filter(Post.id == _id).one_or_none()
        if not post:
            abort(404, detail='No Such Post', passthrough='json')
        # Avoid impersonation
        if post.account_id != int(request.environ.get('HTTP_ACCOUNT')):
            abort(401, detail='Impersonation detected! this incident will be reported', passthrough='json')
        [post.__setattr__(k, v) for k, v in params.items()]
        try:
            DBSession.flush()
        except IntegrityError:
            DBSession.rollback()

    @expose('json')
    @authorize
    def vote(self, post_id, value):
        if not represents_int(post_id):
            abort(400, detail='post_id must be int', passthrough='json')
        if value not in ('Up', 'Down'):
            abort(400, detail='Either vote Down or Up!', passthrough='json')
        account_id = int(request.environ['HTTP_ACCOUNT'])
        # Impose restriction
        require_rep = int(config.get('min_rep_down'))
        if value == 'Down' and DBSession.query(Account).filter(Account.id == account_id).one().reputation < require_rep:
            abort(
                403,
                detail='You must have at least {} reputation to be able to vote-down'.format(require_rep),
                passthrough='json'
            )
        post = DBSession.\
            query(Post).\
            options(joinedload('accounts')). \
            options(joinedload('votes')). \
            filter(Post.id == post_id).\
            one_or_none()
        if not post:
            abort(404, detail='post not found!', passthrough='json')
        if post.account_id == account_id:
            abort(409, detail='You can\'t vote on your own question')
        if post.post_type == 'Comment':
            abort(400, detail='Comment is not subscribed to voting')
        award = int(config.get('question_award')) if post.post_type == 'Question' else int(config.get('answer_award'))
        award = -award if value == 'Down' else award
        for v in post.votes:
            if v.account_id == account_id and v.post_id == int(post_id) and v.value != value:
                DBSession.delete(v)
                post.accounts.reputation = Account.reputation + award
                return
        post.votes.append(Vote(account_id=account_id, post_id=post.id, value=value))
        post.accounts.reputation = Account.reputation + award
        try:
            DBSession.flush()
        except IntegrityError:
            DBSession.rollback()
            abort(400, detail='You can\'t vote twice', passthrough='json')
