# -*- coding: utf-8 -*-
"""Account controller module"""
from os import remove, path
from cgi import FieldStorage

from sqlalchemy.exc import DataError
from sqlalchemy.orm import joinedload
from sqlalchemy_media.exceptions import DimensionValidationError, ContentTypeValidationError, \
    MaximumLengthIsReachedError, MinimumLengthIsNotReachedError
from sqlalchemy_media.stores import StoreManager
from tg import expose, abort, request, config
from tg.controllers.restcontroller import RestController

from rest_ave.model import DBSession, Account, Picture
from rest_ave.model.account import OfficialDocument, OfficialDocuments
from rest_ave.decorators import authorize, increment_account_view, ensure_json_request
from rest_ave.lib.helpers import represents_int, is_image


IMAGES = path.abspath(path.join(path.dirname(__file__), '..', 'public', config.get('storage_path'), 'images'))


class AccountController(RestController):

    @expose('json')
    @authorize
    @increment_account_view
    def get_one(self, account_id):
        with StoreManager(DBSession):
            account = DBSession.query(Account).\
                filter(Account.id == account_id).\
                options(joinedload('viewers')). \
                one_or_none()
        if not account:
            abort(status_code=404, detail='No such user', passthrough='json')
        return dict(account=account)

    @expose('json')
    @authorize
    def upload_image(self, **kw):
        account_id = int(request.environ.get('HTTP_ACCOUNT'))
        image = kw.get('image')
        if not is_image(image):
            abort(400, detail='Image must be a FieldStorage, but %s is provided' % type(image), passthrough='json')
        with StoreManager(DBSession):
            if represents_int(account_id):
                account = DBSession.query(Account).filter(Account.id == account_id).one_or_none()
                if not account:
                    abort(404, detail='No such user!', passthrough='json')
                # Remove the previous one if already has one
                if account.image:
                    self._remove_image(account.image.get('key'))
                try:
                    account.image = Picture.create_from(image.file)
                except DimensionValidationError:
                    abort(400, detail='Image must be at most 3264 x 2448', passthrough='json')
            else:
                abort(400, detail='account_id must be int', passthrough='json')

    @expose()
    @authorize
    def remove_image(self):
        account_id = int(request.environ.get('HTTP_ACCOUNT'))
        with StoreManager(DBSession):
            account = DBSession.query(Account).filter(Account.id == account_id).one_or_none()
            image_key = account.image.get('key')
            account.image = None
            self._remove_image(image_key)

    def _remove_image(self, key):
        filename = 'image-{}.jpg'.format(key)
        path = '{}/{}'.format(IMAGES, filename)
        try:
            remove(path)
        except FileNotFoundError:
            pass

    @expose('json')
    @authorize
    def upload_document(self, **kw):
        account_id = int(request.environ.get('HTTP_ACCOUNT'))
        document = kw.get('document')
        if not isinstance(document, FieldStorage):
            abort(400, detail='document must be a FieldStorage, but %s is provided' % type(document), passthrough='json')
        if represents_int(account_id):
            with StoreManager(DBSession):
                account = DBSession.query(Account).filter(Account.id == account_id).one_or_none()
                if not account:
                    abort(404, detail='No such user!', passthrough='json')
                try:
                    account.official_documents = OfficialDocuments()
                    account.official_documents.append(OfficialDocument.create_from(document.file))
                except ContentTypeValidationError:
                    abort(400, detail='Only image/jpeg and pdf are allowed', passthrough='json')
                except (MaximumLengthIsReachedError, MinimumLengthIsNotReachedError):
                    abort(400, detail='File must be at at least 10MB, at most 2MB', passthrough='json')
        else:
            abort(400, detail='account_id must be int', passthrough='json')

    @expose('json')
    @authorize
    @ensure_json_request
    def put(self, **kwargs):
        account_id = int(request.environ.get('HTTP_ACCOUNT'))
        bio = kwargs.get('bio')
        if not account_id or not bio or len(kwargs.keys()) > 2:
            abort(400, detail='only bio and id must be provided', passthrough='json')
        account = DBSession.query(Account).filter(Account.id == account_id).one_or_none()
        if not account:
            abort(404, detail='No Such Post', passthrough='json')
        account.bio = bio
        try:
            DBSession.flush()
        except DataError:
            abort(400, detail='Bio too long! Must be at most 100 characters', passthrough='json')

    @expose('json')
    @authorize
    def total(self):
        return dict(total=len(DBSession.query(Account).filter(Account.is_official == True).all()))
