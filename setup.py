# -*- coding: utf-8 -*-

# This is just a work-around for a Python2.7 issue causing
# interpreter crash at exit when trying to log an info message.

# TODO: make sure to sudo apt-get install libmagickwand-dev
# TODO: pip install git+git://github.com/bbangert/beaker_extensions.git
from setuptools import setup, find_packages

testpkgs = [
    'WebTest >= 1.2.3',
    'nose',
    'coverage',
    'gearbox',
]

install_requires = [
    "TurboGears2 >= 2.3.9",
    "Beaker >= 1.8.0",
    "Kajiki >= 0.3.5",
    "Mako",
    "zope.sqlalchemy >= 0.4",
    "sqlalchemy",
    "alembic",
    "repoze.who",
    "tw2.forms",
    "tgext.admin >= 0.6.1",
    "tgext.routes",
    "wand",
    "python-magic",
    "redis",
    "pydes",
    "sqlalchemy-media",
    "webhelpers2",
    "psycopg2",
    "suds-jurko"
]
setup(
    name='rest-ave',
    version='0.1',
    description='',
    author='',
    author_email='',
    url='',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=install_requires,
    include_package_data=True,
    test_suite='nose.collector',
    tests_require=testpkgs,
    package_data={'rest_ave': [
        'i18n/*/LC_MESSAGES/*.mo',
        'templates/*/*',
        'public/*/*'
    ]},
    message_extractors={'rest_ave': [
        ('**.py', 'python', None),
        ('templates/**.mak', 'mako', None),
        ('templates/**.xhtml', 'kajiki', None),
        ('templates/**.mako', 'mako', None),
    #        ('public/**', 'ignore', None)
    ]},
    entry_points={
        'paste.app_factory': [
            'main = rest_ave.config.middleware:make_app'
        ],
        'gearbox.plugins': [
            'turbogears-devtools = tg.devtools'
        ]
    },
    zip_safe=False
)
